//@ts-check

const { parseResponse, parseApiResult } = require('../utils/helper.js');
const { Group, NewGroup } = require('../api/groups.js');
const { User, AuthUser } = require('../api/user.js');
const express = require('express');
const router = express.Router();

function hasPermissions(req, res, next) {
  if (true) {
    const { userKey } = req.body;
    console.log(userKey, 'has permissions checked...');
    res.locals.userKey = userKey;
    next();
  } else {
    res.status(500).json({ error: 'user has no permissions' });
  }
}

router.post('/createUser', hasPermissions, async (req, res) => {
  const userKey = res.locals.userKey;
  const { name, type } = req.body;
  if (type === 'STAFF') {
    const user = User({ userType: 'staffs' });
    const result = await user.createUser({ name, userKey });
    parseApiResult(res, result);
  } else if (type === 'PATIENT') {
    const user = User({ userType: 'patients' });
    const result = await user.createUser({ name, userKey });
    parseApiResult(res, result);
  } else {
    return { err: 'must be staff or patient only', payload: '' };
  }
});

router.post('/getUserGroups', async (req, res) => {
  const userKey = res.locals.userKey;
  const { userType } = req.body;
  const user = AuthUser({ userKey, userType });
  const result = await user.userGroups();
  parseApiResult(res, result);
});

router.post('/deleteDocument');

router.get('/getUserDocuments');

router.get('/getUserProfile');

router.get('/getAllUsers', async (req, res) => {
  const { userType } = req.body;
  const user = User({ userType });
  const result = await user.getAllUsers();
  parseApiResult(res, result);
});

module.exports = router;
