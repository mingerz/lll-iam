//@ts-check
const { db } = require('../arangoConn/arangoConn.js');
const graph = db.graph('patient-healthcare');
var bodyParser = require('body-parser');
const express = require('express');
const app = express();
const port = 3000;
const userRoute = require('./user.route');
const groupRoute = require('./group.route');
const docRoute = require('./doc.route');

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

function authenticate(req, res, next) {
  const { uid } = req.body;
  if (uid === '') {
    res.status(500).json({ error: 'not authenticated' });
  } else {
    res.locals.userKey = uid;
    next();
  }
}

app.use(bodyParser.json());
app.use('/staff', authenticate, userRoute);
app.use('/group', authenticate, groupRoute);
app.use('/doc', authenticate, docRoute);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
