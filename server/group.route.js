//@ts-check

const { parseResponse, parseApiResult } = require('../utils/helper.js');
const { CurrentGroup, Group } = require('../api/groups.js');
const express = require('express');
const router = express.Router();

function hasPermissions(req, res, next) {
  if (true) {
    const { userKey } = req.body;
    console.log(userKey, 'has permissions checked...');
    res.locals.userKey = userKey;
    next();
  } else {
    res.status(500).json({ error: 'user has no permissions' });
  }
}

router.post('/createGroup', hasPermissions, async (req, res) => {
  const { groupName, userCategory } = req.body;
  const userKey = res.locals.userKey;
  const NewG = Group();
  let result = await NewG.createGroup({ groupName });
  const groupKey = result.payload._key;
  const G = CurrentGroup({ groupKey });
  const userAdded = await G.addToGroup({
    userCategory,
    groupMemberKey: userKey,
    hasWriteAccess: true,
    isAdmin: true
  });
  parseApiResult(res, result);
});

router.post('/deleteGroup', async (req, res) => {
  const { groupKey } = req.body;
  const G = CurrentGroup({ groupKey });
  const result = await G.deleteGroup();
  parseApiResult(res, result);
});

router.post('/updateGroupMembership', hasPermissions, async (req, res) => {
  const { hasWriteAccess, isAdmin, userCategory, groupMemberKey, groupKey, action } = req.body;
  const userKey = res.locals.userKey;
  const G = CurrentGroup({ groupKey });
  if (action === 'ADD_USER') {
    const result = await G.addToGroup({
      userCategory,
      groupMemberKey,
      hasWriteAccess,
      isAdmin
    });
    parseApiResult(res, result);
  } else if (action === 'REMOVE_USER') {
    const cursor = await G.removeFromGroup({
      groupMemberKey
    });
    parseApiResult(res, await cursor.next());
  } else {
    parseApiResult(res, { err: 'user is not admin', payload: '' });
  }
});

router.post('/updateGroupMemberPermissions', hasPermissions, async (req, res) => {
  const { isAdmin, hasWriteAccess, groupMemberKey, groupKey } = req.body;
  const result = await CurrentGroup({ groupKey }).updateGroupMemberPermission({
    groupMemberKey,
    payload: { hasWriteAccess, isAdmin }
  });
  parseApiResult(res, result);
});

router.get('/listGroups', async (req, res) => {
  const G = Group();
  const result = await G.listGroups();
  console.log('RESULT: ', result);
  parseApiResult(res, await G.listGroups());
});

router.get('/getGroupDocuments', async (req, res) => {
  const { groupKey } = req.body;
  const G = CurrentGroup({ groupKey });
  const result_files = await G.getGroupDocuments();
  parseApiResult(res, result_files);
});

router.get('/groupMembers', async (req, res) => {
  const { groupKey } = req.body;
  const G = CurrentGroup({ groupKey });
  const result_members = await G.groupMembersList();
  parseApiResult(res, result_members);
});

module.exports = router;
