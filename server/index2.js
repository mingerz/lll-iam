//@ts-check
const { db } = require('../arangoConn/arangoConn.js');
const graph = db.graph('patient-healthcare');
const collection = graph.edgeCollection('documentAccess');
const { NewUser, AuthUser } = require('../api/user');
const { parseApiResult } = require('../utils/helper');
var bodyParser = require('body-parser');
const express = require('express');
const app = express();
const router = express.Router();
const port = 3000;

// router.use(
//   bodyParser.urlencoded({
//     extended: true
//   })
// );

// router.use(bodyParser.json());

router.post('/createUser', async (req, res) => {
  const { name, uid, type } = req.body;
  const { err, payload } = await NewUser().createUser({ name, userKey, type });
  parseApiResult(res, { err, payload });
});

// router.post('/staff/:uid/*', (req, res, next) => {

//   const id = req.params.uid;
//   if (id === undefined) {
//     res.status(400).json({ error: 'no id is specified' });
//   }
//   res.locals.uid = id;
//   next();
// });


router.post('/staff/:uid/getUserGroups', async (req, res) => {});

router.post('/staff/:uid/createDocument', async (req, res) => {
  const { documentID } = req.body;
  const uid = res.locals.uid;
  const { err, payload } = await AuthUser({ userKey: uid }).createFile({ documentID });
  parseApiResult(res, { err, payload });
});

);

router.post('/staff/:uid/addGroupMember', async (req, res) => {
  const { isAdmin, hasWriteAccess, groupMemberKey, groupKey, action } = req.body;
  const uid = res.locals.uid;
  const { err, payload } = await AuthUser({ userKey: uid }).updateGroupMembers({
    groupKey,
    action: 'ADD_USER',
    groupMemberKey,
    isAdmin,
    hasWriteAccess
  });
  parseApiResult(res, { err, payload });
});

router.post('/staff/:uid/updateGroupMemberPermissions', async (req, res) => {
  const { isAdmin, hasWriteAccess, groupMemberKey, groupKey } = req.body;
  const uid = res.locals.uid;
  const { err, payload } = await AuthUser({ userKey: uid }).updateMemberPermissions({
    groupKey,
    groupMemberKey,
    hasWriteAccess,
    isAdmin
  });
  parseApiResult(res, { err, payload });
});

router.get('/group/:id/groupProfile', async (req, res) => {});

router.get('/group/:id/getGroupMembers', async (req, res) => {
  const { groupKey } = req.body;
  const uid = res.locals.uid;
  const { err, payload } = await AuthUser({ userKey: uid }).getGroupMembers({ groupKey });
  parseApiResult(res, { err, payload });
});

router.get('/company/getUsers');

// router.listen(port, () => console.log(`Example router listening on port ${port}!`));
module.exports = router;
