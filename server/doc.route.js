//@ts-check

const { parseResponse, parseApiResult } = require('../utils/helper.js');
const { Doc, CurrentDoc } = require('../api/doc.js');
const express = require('express');
const router = express.Router();

// async function checkPermissions({ documentKey, userKey, hasWriteAccess, isAdmin }) {
//   const D = CurrentDoc({ documentKey });
//   const { err, payload } = await D.getDocumentAdmins();
//   if (payload.includes(userKey)) {
//     return true;
//   }
//   const isAdmin = G.isAdmin();
//   const isFileCreator = D.isFileCreator();
//   const hasGroupAccess = G.hasGroupWriteAccess();
//   return {
//     isAdmin,
//     isFileCreator,
//     hasGroupWriteAccess
//   };
//   if (isAdmin) {
//     return { isAdmin: true, isFileCreator: true, hasGroupWriteAccess: false };
//   } else if (isFileCreator) {
//     return { isAdmin: true, isFileCreator: true, hasGroupWriteAccess: false };
//   } else if (hasGroupWriteAccess) {
//   } else {
//     return { error: 'does not have permissions', payload: false };
//   }
// }

function hasPermissions(req, res, next) {
  if (true) {
    const { userKey } = req.body;
    console.log(userKey, 'has permissions checked...');
    res.locals.userKey = userKey;
    next();
  } else {
    res.status(500).json({ error: 'user has no permissions' });
  }
}

router.post('/createDocument', hasPermissions, async (req, res) => {
  const { userCategory, documentKey } = req.body;
  const userKey = res.locals.userKey;
  const D = Doc();
  const docCreated = await D.createDoc({ documentKey });
  const docKey = docCreated.payload._key;
  console.log('DOCUMENTKEY ==: ', docKey);
  const CurrDoc = CurrentDoc({ documentKey });
  const { err, payload } = await CurrDoc.createDocPermissions({
    userCategory,
    toAccessKey: userKey,
    hasWriteAccess: true,
    isAdmin: true
  });
  if (err === '') {
    parseApiResult(res, docCreated);
  }
});

router.post('/deleteFile');

router.post('/updateDocPermissions', async (req, res) => {
  console.log('UPDATING DOC');
  const { userCategory, documentKey, toAccessKey, hasWriteAccess, isAdmin } = req.body;
  const D = CurrentDoc({ documentKey });
  const filePermissions_result = await D.updateDocPermissions({
    userCategory,
    toAccessKey,
    hasWriteAccess,
    isAdmin
  });
  parseApiResult(res, filePermissions_result);
});

router.post('/createDocAccess', async (req, res) => {
  const { userCategory, documentKey, toAccessKey, hasWriteAccess, isAdmin } = req.body;
  const D = CurrentDoc({ documentKey });
  const filePermissions_result = await D.createDocPermissions({
    userCategory,
    toAccessKey,
    hasWriteAccess,
    isAdmin
  });
  parseApiResult(res, filePermissions_result);
});

router.post('/getDocumentAdmins', async (req, res) => {
  const userKey = res.locals.userKey;
  const { documentKey } = req.body;
  const D = CurrentDoc({ documentKey });
  const docAdmins_result = await D.getDocumentAdmins();
  parseApiResult(res, docAdmins_result);
});

module.exports = router;
