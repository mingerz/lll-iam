//@ts-check
const { db } = require('../arangoConn/arangoConn.js');
const graph = db.graph('patient-healthcare');
const collection = graph.edgeCollection('documentAccess');
const { ColMutations, ColQueries, UserMutations } = require('../api/index.js');

var bodyParser = require('body-parser');
const express = require('express');
const app = express();
const port = 3000;

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(bodyParser.json());

// app.get('/', (req, res) => {
//   console.log(req.body);
//   res.send('Hello World!');
// });
// app.post('/', (req, res) => {
//   console.log(req.body);
//   console.log(Object.keys(req.body));
//   res.json({ status: 200 });
// });

// app.post('/staff/:id/*', (req, res, next) => {
//   const { name, type, rank, userKey } = req.body;
//   const user = new UserMutations('staff', db, graph, name, type, rank, userKey);
//   res.locals.user = user;
//   next();
// });

app.post('/createUser', async (req, res) => {
  const { name, type, rank } = req.body;
  const user = new UserMutations({ collection: 'staff', db, graph, name, type, rank });
  const { err, payload } = await user.createNewUser();
  res.json({ err, payload });
});

app.post('/patients/createPatient');

app.post('/company/getUsers');

app.post('/staff/:uid/*', (req, res, next) => {
  const id = req.params.uid;
  if (id === undefined) {
    res.json({ statusCode: 400, message: 'not authtorized. please login' });
  }
  res.locals.uid = id;
  next();
});

app.post('/staff/:uid/createDocument', async (req, res) => {
  const { name, type, documentName } = req.body;
  const uid = res.locals.uid;
  console.log('UID: ', uid);
  const user = new UserMutations({ collection: 'staff', db, graph, name, type, userKey: uid });
  const result = await user.createDocument({ documentName });
  res.json(result);
});

app.post('/staff/:id/addGroupMember', async (req, res) => {
  const user = res.locals.user;
  const result = await staffInit2.addGroupMember({
    staffKey: payload3._key,
    groupKey: group1._key,
    hasWriteAccess: 'false',
    isAdmin: 'false'
  });
  res.json(result);
});

app.post('/staff/:id/removeGroupMember', async (req, res) => {
  const { groupUID } = req.body;
});

app.get('/staff/:id/userGroupPermissions', async (req, res) => {
  const { groupUID } = req.body;
  const result = await user.getUserGroupPermissions({ groupUID, userUID: res.locals.uid });
  res.json(result);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
