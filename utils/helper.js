//@ts-check
const parseResponse = result => {
  if (typeof result == 'object' && result !== null) {
    return { err: '', payload: result };
  } else if (Object.keys(result).includes('payload') && Object.keys(result).includes('err')) {
    return result;
  } else if (Array.isArray(result)) {
    if (result.length === 0) {
      return { err: 'result length was zero', payload: {} };
    }
    return { err: '', payload: result };
  } else {
    const { err, payload } = result;
    if (err !== '') {
      return { err: 'result length was zero', payload: {} };
    } else {
      return { err: '', payload: payload };
    }
  }
};

const parseApiResult = (res, { err, payload }) => {
  if (err !== '') {
    res.status(400).json({ error: err });
  } else {
    res.status(200).json(payload);
  }
};

module.exports = {
  parseResponse,
  parseApiResult
};
