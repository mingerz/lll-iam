const { Database, aql } = require('arangojs');
const db = new Database();
db.useDatabase('test');
db.useBasicAuth('root', '0Me3BZd106D8OrTO');

module.exports = {
  db,
  aql
};
