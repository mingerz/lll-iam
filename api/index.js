const { ColMutations } = require('./mutations.js');
const { UserMutations } = require('./userMutations.js');
const { ColQueries } = require('./queries.js');

module.exports = {
  ColMutations,
  ColQueries,
  UserMutations
};
