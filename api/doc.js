//@ts-check
const { Base } = require('./arangoModels.js');
const { parseResponse } = require('../utils/helper.js');
const { db } = require('../arangoConn/arangoConn.js');
var arangojs = require('arangojs');
const aql = arangojs.aql;

function Doc() {
  const createDoc = async ({ documentKey }) => {
    const cursor = await Base({ collection: 'documents' }).insertIntoCollection({
      _key: documentKey
    });
    const result = await cursor.next();
    return parseResponse(result);
  };
  return Object.freeze({
    createDoc
  });
}
function CurrentDoc({ documentKey }) {
  const deleteFile = async () => {
    const cursor = await Base({ collection: 'documents' }).removeVertex({ key: documentKey });
    const result = await cursor.next();
    parseResponse(result);
  };

  /**
   *
   * @param {{userCategory: string, toAccessKey: string, hasWriteAccess: boolean, isAdmin: boolean}} param0
   */
  const updateDocPermissions = async ({
    userCategory = 'staffs',
    toAccessKey,
    hasWriteAccess = false,
    isAdmin = false
  }) => {
    const payload = {
      hasWriteAccess: Boolean(hasWriteAccess),
      isAdmin: Boolean(isAdmin)
    };

    const documentID = `documents/${documentKey}`;
    const toUserID = `${userCategory}/${toAccessKey}`;
    console.log(documentID, toUserID);
    const cursor = await db.query(aql`
      FOR v, e IN ANY ${documentID} GRAPH 'patient-healthcare' 
      FILTER e._to == ${toUserID}
      RETURN e._key
      `);
    const edgeKey = await cursor.next();
    //TODO: if existing connection does not exist, handle error
    console.log(edgeKey, '================');
    const updateCursor = await db
      .query(aql` UPDATE {_key: ${edgeKey}} WITH ${payload} IN documentDirectory RETURN NEW`)
      .catch(err => err);
    const result = await updateCursor.next();
    return parseResponse(result);
  };

  const createDocPermissions = async ({
    hasWriteAccess,
    isAdmin,
    toAccessKey,
    userCategory = 'staffs'
  }) => {
    const to = `${userCategory}/${toAccessKey}`;
    const from = `documents/${documentKey}`;
    const payload = {
      hasWriteAccess: Boolean(hasWriteAccess),
      isAdmin: Boolean(isAdmin),
      _from: from,
      _to: to
    };
    const cursor = await Base({ collection: 'documentDirectory' }).insertIntoCollection(payload);
    return parseResponse(await cursor.next());
  };

  const getDocumentAdmins = async () => {
    const documentID = `documents/${documentKey}`;
    const cursor = await db.query(aql`
        FOR v, edge, p IN 1..1 ANY ${documentID} documentDirectory 
        OPTIONS {
            bfs: true, 
            uniqueVertices: 'global', 
            uniqueEdges: 'path'
        }
        FILTER edge.isAdmin == true
            RETURN edge
      `);
    return parseResponse(await cursor.all());
  };

  return Object.freeze({
    deleteFile,
    updateDocPermissions,
    getDocumentAdmins,
    createDocPermissions
  });
}

module.exports = { Doc, CurrentDoc };
