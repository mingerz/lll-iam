//@ts-check
var arangojs = require('arangojs');
const aql = arangojs.aql;

class ColMutations {
  constructor(collection, db, graphDB) {
    this.db = db;
    this.collection = db.collection(collection);
    this.graphCollection = graphDB.vertexCollection(collection);
  }
  async insertIntoCollection(payload) {
    if (Array.isArray(payload)) {
      const result = await this.db.query(aql`
        FOR doc IN ${payload}
          UPSERT doc
          INSERT doc
          UPDATE {}
          INTO ${this.collection}
          RETURN NEW
      `);
      return result;
    } else {
      const result = await this.db.query(aql`
        UPSERT ${payload}
        INSERT ${payload}
        UPDATE {}
        INTO ${this.collection}
        RETURN NEW
      `);
      return result;
    }
  }

  /**
   *
   * @param {{key: string}} param0
   */
  async removeVertex({ key }) {
    const deleted = await this.graphCollection.remove(key);
    return deleted;
  }

  /**
   *
   * @param {{key: string}} param0
   */
  async removeEdge({ key }) {
    const deleted = await this.collection.remove(key);
    return deleted;
  }

  /**
   *
   * @param {{name: string, rank: string, type: string}} param0
   */
  async createNewUser({ name, rank, type }) {
    return await this.insertIntoCollection({ name, rank, type });
  }

  async emptyCollection() {
    const result = await this.db.query(aql`
      FOR i in ${this.collection} 
        REMOVE i._key IN ${this.collection}
    `);
    return result;
  }

  /**
   *
   * @param {{from: string, to: string}} param0
   */
  async createEdge({ from, to }) {
    const result = await this.db.query(
      aql` INSERT {_from: ${from}, _to: ${to}} INTO ${this.collection}`
    );
    return result;
  }

  /**
   *
   * @param {{from: string, to: string, hasWriteAccess: boolean, isAdmin: boolean}} param0
   */
  async createEdges({ from, to, hasWriteAccess, isAdmin }) {
    const result = await this.db.query(
      aql` INSERT {_from: ${from}, _to: ${to}, hasWriteAccess: ${hasWriteAccess}, isAdmin: ${isAdmin}} INTO ${this.collection} RETURN NEW`
    );
    return result;
  }

  /**
   *
   * @param {{groupName: string}} param0
   */
  async createGroup({ groupName }) {
    return await this.insertIntoCollection({ _key: `${groupName}`, name: groupName });
  }

  /**
   *
   * @param {{key: string}} param0
   */
  async deleteGroup({ key }) {
    return await this.removeVertex({ docuemntKey: key });
  }

  async giveAccessToGroup({ docKey, hasAccessTo, hasWriteAccess, isAdmin }) {
    const result = await this.db.query(
      aql` 
      INSERT {_from: ${docKey}, _to: ${hasAccessTo}, hasWriteAccess: ${hasWriteAccess}, isAdmin: ${isAdmin}} INTO ${this.collection} RETURN NEW

      `
    );
    return result;
  }

  async addUserToDepartment({ user, department }) {
    const userFilter = aql`FILTER u.userName==${user}`;
    const departmentFilter = aql`FILTER u.departmentName==${department}`;
    const userBon = await queryCollection(employeesCollection, userFilter);
    const employeeID = userBon._result[0]._id;

    const departmentFinance = await queryCollection(departmentCollection, departmentFilter);
    const departmentID = departmentFinance._result[0]._id;

    // CREATE EDGES COLLECTION between employees and departments
    const edgeData = await createEdge({
      collection: departmentStaff,
      to: departmentID,
      from: employeeID
    });
    return edgeData;
  }
}

module.exports = {
  ColMutations
};
