class Animal {
  constructor(type) {
    this.type = type;
  }

  getType() {
    return new Promise(resolve => {
      resolve(this.type);
    });
  }
}

class Cat extends Animal {
  constructor(name) {
    super('mee');
    const randomNum = () => {
      const val = super.getType();
      return val;
    };

    this.name = name;
    this.random = randomNum();
  }

  getRandom() {
    return this;
  }
}

var c = new Cat('yellow');
const { type, name } = c;
console.log(type, name);
console.log(typeof c);
console.log(Object.values(c));
console.log(Object.keys(c));
console.log(c.getRandom());
