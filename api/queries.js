var arangojs = require('arangojs');
const aql = arangojs.aql;

class ColQueries {
  constructor(collection, db, graphDB) {
    this.db = db;
    this.collection = db.collection(collection);
    this.graphCollection = graphDB.vertexCollection(collection);
  }
  async queryCollection({ filterFunc = undefined }) {
    const users = await this.db.query(
      aql`
    FOR u IN ${this.collection} 
      ${filterFunc} 
      RETURN u
    `
    );
    return users;
  }

  async findColleagues() {
    const result = await this.db.query(
      aql`FOR v, e, p in 1..2 ANY 'employees/a1' GRAPH 'org-chart'
        RETURN {vertices: v._key, edges: e._key, path: p._key}
      `
    );
    return result;
  }
}

module.exports = {
  ColQueries
};
