//@ts-check
const { db } = require('../arangoConn/arangoConn.js');
const { parseResponse } = require('../utils/helper.js');
const { Base } = require('./arangoModels');
const aql = require('arangojs').aql;

function Group() {
  /**
   *  Creating a group
   * @param {{groupName: string}} param0
   * @returns {Promise<{err: string, payload: object}>}
   */
  const createGroup = async ({ groupName }) => {
    const cursor = await Base({ collection: 'groups' }).insertIntoCollection({
      name: groupName
    });
    const result = await cursor.next();
    return parseResponse(result);
  };

  const listGroups = async () => {
    const cursor = await db.query(aql`
      FOR g IN groups
        RETURN g
    `);
    const result = await cursor.all();
    return parseResponse(result);
  };

  return Object.freeze({
    createGroup,
    listGroups
  });
}

function CurrentGroup({ groupKey }) {
  /**
   *
   * @param {{userCategory: string, groupMemberKey: string, hasWriteAccess: boolean, isAdmin: boolean}} param0
   */
  const addToGroup = async ({
    userCategory,
    groupMemberKey,
    hasWriteAccess = false,
    isAdmin = false
  }) => {
    const cursor = await Base({ collection: 'groupDirectory' }).insertIntoCollection({
      _from: `groups/${groupKey}`,
      _to: `${userCategory}/${groupMemberKey}`,
      hasWriteAccess: Boolean(hasWriteAccess),
      isAdmin: Boolean(isAdmin)
    });
    const result = await cursor.next();
    return parseResponse(result);
  };

  /**
   *
   * @param {{groupMemberKey: string}} param0
   */
  const removeFromGroup = async ({ groupMemberKey }) => {
    const cursor = await Base({ collection: 'groupDirectory' }).removeEdge({
      from: groupKey,
      to: groupMemberKey
    });
    return parseResponse(await cursor.next());
  };

  const deleteGroup = async () => {
    const cursor = await Base({ collection: 'groups' }).removeVertex({ key: groupKey });
    return parseResponse(await cursor.next());
  };

  /**
   *
   * @param {{groupMemberKey: string, payload: object}} param0
   */
  const updateGroupMemberPermission = async ({ groupMemberKey, payload }) => {
    const cursor = await db.query(
      aql` UPDATE ${groupMemberKey} WITH ${payload} IN groupDirectory RETURN NEW`
    );
    return parseResponse(await cursor.next());
  };

  /**
   *
   * @param {{groupKey: string}} param0
   */
  const groupMembersCount = async () => {
    const cursor = await db.query(
      aql`RETURN LENGTH (FOR doc IN groupDirectory FILTER doc._from == ${groupKey} RETURN 1)`
    );
    return parseResponse(await cursor.next());
  };

  const getGroupDocuments = async () => {
    return parseResponse();
  };

  const groupMembersList = async () => {
    const groupId = `groups/${groupKey}`;
    const cursor = await db.query(
      aql`
      FOR v, edge, p IN 1..1 OUTBOUND ${groupId} groupDirectory
        OPTIONS {
            bfs: true, 
            uniqueVertices: 'global', 
            uniqueEdges: 'path'
        }
       RETURN v
      `
    );
    return parseResponse(await cursor.all());
  };

  const getGroupAdmins = async ({ groupKey }) => {
    const res = await this.db.query(aql`
        FOR v, edge, p IN 1..2 ANY ${groupKey} groupDirectory
        OPTIONS {
            bfs: true, 
            uniqueVertices: 'global', 
            uniqueEdges: 'path'
        }
        FILTER edge.isAdmin == true
            RETURN edge
      `);
    const result = res._result;
    return parseResponse(result);
  };

  /**
   *
   * @param {{groupKey: string, userKey: string}} param0
   */
  const getMemberPermissions = ({ userKey }) => {
    return { err: '', payload: { groupKey, userKey, hasWriteAccess: 'true', isAdmin: 'true' } };
  };

  /**
   *
   * @param {{groupMemberKey: string}} param0
   */
  const checkIfAdmin = async ({ groupMemberKey }) => {
    console.log('checking if admin. GroupKey: ', groupKey, 'memberKey: ', groupMemberKey);
    const groupId = `groups/${groupKey}`;
    const staffId = `staffs/${groupMemberKey}`;
    const cursor = await db.query(aql`
      FOR v, edge, p IN 1..2 ANY ${groupId} groupDirectory
        OPTIONS {
            bfs: true, 
            uniqueVertices: 'global', 
            uniqueEdges: 'path'
        }
        FILTER edge.isAdmin == true 
        AND edge._to == ${staffId}
        RETURN edge
      `);
    const result = await cursor.next();
    if (result === undefined) {
      return false;
    }
    return result.isAdmin == true;
  };

  const checkPermissions = ({ userKey }) => {
    return true;
  };

  return Object.freeze({
    addToGroup,
    checkIfAdmin,
    removeFromGroup,
    groupMembersCount,
    groupMembersList,
    getMemberPermissions,
    deleteGroup,
    updateGroupMemberPermission,
    checkPermissions,
    getGroupDocuments,
    getGroupAdmins
  });
}

module.exports = {
  CurrentGroup,
  Group
};
