const { db } = require('../arangoConn/arangoConn.js');
const graph = db.graph('patient-healthcare');
var arangojs = require('arangojs');
const { ColMutations } = require('./mutations.js');
const aql = arangojs.aql;
const { parseResponse } = require('../utils/helper.js');

class GroupMutations extends ColMutations {
  constructor(collection, db, graphDB) {
    super(collection, db, graphDB);
  }

  async deleteGroup({ key }) {
    return await super.removeVertex({ key });
  }

  async createGroup({ groupName }) {
    return await super._insertIntoCollection({ groupName });
  }

  async getGroupAdmins({ groupKey }) {
    const res = await this.db.query(aql`
        FOR v, edge, p IN 1..2 ANY ${groupKey} groupDirectory
        OPTIONS {
            bfs: true, 
            uniqueVertices: 'global', 
            uniqueEdges: 'path'
        }
        FILTER edge.isAdmin == true
            RETURN edge
      `);
    const result = res._result;
    return parseResponse(result);
  }

  async checkIfAdmin({ groupKey, staffKey }) {
    const groupID = `groups/${groupKey}`;
    const staffID = `staff/${staffKey}`;
    const res = await this.db.query(aql`
      FOR v, edge, p IN 1..2 ANY ${groupID} groupDirectory
        OPTIONS {
            bfs: true, 
            uniqueVertices: 'global', 
            uniqueEdges: 'path'
        }
        FILTER edge.isAdmin == true 
        AND edge._to == ${staffID}
        RETURN edge
      `);
    const result = res._result;
    return parseResponse(result);
  }

  async addMember({ groupKey, staffKey, hasWriteAccess, isAdmin }) {
    const res = await this._insertIntoCollection({
      _from: `groups/${groupKey}`,
      _to: `staff/${staffKey}`,
      hasWriteAccess,
      isAdmin
    });
    const result = res._result;
    return parseResponse(result);
  }
}

module.exports = {
  GroupMutations
};
