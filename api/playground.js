//@ts-check

/**
 *
 * @param {{x: number, y: number}} param0
 * @returns{number}
 */
function foo({ x, y }) {
  return x * y;
}

foo({ x: 'sdf', y: 3 });
const a = foo({ x: 3, y: 3 });
console.log(a + 'hello');
