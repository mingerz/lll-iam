//@ts-check
const { Base } = require('./arangoModels.js');
const { parseResponse } = require('../utils/helper.js');
const { db } = require('../arangoConn/arangoConn.js');
const { Group, NewGroup } = require('./groups.js');
var arangojs = require('arangojs');
const aql = arangojs.aql;

/**
 *
 * @typedef {'staffs'|'patients'} userType
 * @param {{userType}} param0
 */
function User({ userType }) {
  /**
   * @param {{name: string, userKey: string}} param0
   */
  const createUser = async ({ name, userKey }) => {
    const cursor = await Base({ collection: userType }).insertIntoCollection({
      name,
      _key: userKey
    });
    return parseResponse(await cursor.next());
  };

  const getAllUsers = async () => {
    const cursor = await db.query(aql`
      FOR u IN ${userType} 
        RETURN u 
    `);
    console.log(cursor);
    return parseResponse(await cursor.all());
  };

  return Object.freeze({ createUser, getAllUsers });
}

/**
 * @param {{userKey: string, userType: userType }} param0
 */
function AuthUser({ userKey, userType }) {
  const userGroups = async () => {
    const cursor = await db.query(aql`
      FOR s IN ${userType} 
      FILTER s._key == ${userKey}
      FOR group, groupAccess IN 1..1 ANY s groupDirectory
          RETURN {group, groupAccess}
    `);
    const result = await cursor.all();
    return parseResponse(result);
  };

  /**
   *
   * @param {{documentID: string}} param0
   */
  const createFile = async ({ documentID }) => {
    const docCol = Base({ collection: 'documents' });
    const docAccessEdge = Base({ collection: 'documentAccess' });
    const documentInsertedCursor = await docCol.insertIntoCollection({
      _key: documentID
    });
    const { _key: documentKey } = await documentInsertedCursor.next();
    const cursor = await docAccessEdge.createEdges({
      from: `${userType}/${userKey}`,
      to: `documents/${documentKey}`,
      hasWriteAccess: true,
      isAdmin: true
    });
    return parseResponse(await cursor.next());
  };

  return Object.freeze({
    userGroups,
    createFile
  });
}

module.exports = {
  User,
  AuthUser
};
