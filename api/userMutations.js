const { db } = require('../arangoConn/arangoConn.js');
const graph = db.graph('patient-healthcare');
var arangojs = require('arangojs');
const { ColMutations } = require('./mutations.js');
const { GroupMutations } = require('./groupMutations.js');
const aql = arangojs.aql;

const { parseResponse } = require('../utils/helper.js');

class UserMutations extends ColMutations {
  constructor({ collection, db, graphDB, name = '', type = '', rank = '', userKey = '' }) {
    super(collection, db, graphDB);
    this.name = name;
    this.rank = rank;
    this.type = type;
    this.docAccessCollection = new ColMutations('documentAccess', db, graph);
    this.documentsCollection = new ColMutations('documents', db, graph);
    this.groupsCollection = new GroupMutations('groups', db, graph);
    this.groupsDirectory = new GroupMutations('groupDirectory', db, graph);
    this.userKey = userKey;
  }

  async createNewUser() {
    const result = await super._insertIntoCollection({
      name: this.name,
      rank: this.rank,
      type: this.type
    });
    return parseResponse(result._result);
  }

  async createDocument({ documentName }) {
    if (this.userKey === '') {
      throw new Error('there is no user initialised. userKey is empty');
    }
    const documentInserted = await this.documentsCollection._insertIntoCollection({
      name: documentName
    });
    const { _key: documentKey } = documentInserted._result[0];
    const linkCreated = await this.docAccessCollection.createEdges({
      from: `staff/${this.userKey}`,
      to: `documents/${documentKey}`,
      hasWriteAccess: true,
      isAdmin: true
    });
    return parseResponse(linkCreated._result);
  }

  async deleteGroup({ groupKey }) {
    const userIsGroupAdmin = await this.groupsDirectory.checkIfAdmin({
      staffKey: this.userKey,
      groupKey
    });
    if (userIsGroupAdmin) {
      return this.groupsCollection.deleteGroup({ key: groupKey });
    }
  }

  async createGroup({ groupName }) {
    if (this.userKey === '') {
      throw new Error('there is no user initialised. userKey is empty');
    }
    const groupInserted = await this.groupsCollection.createGroup({
      groupName
    });
    const { _key: groupKey } = groupInserted._result[0];
    const groupMemberAdded = await this.addGroupMember({
      staffKey: this.userKey,
      groupKey,
      hasWriteAccess: true,
      isAdmin: true
    });
    return parseResponse(groupInserted._result);
  }

  async addGroupMember({ staffKey, groupKey, hasWriteAccess, isAdmin }) {
    const userIsGroupAdmin = await this.groupsDirectory.checkIfAdmin({
      staffKey: this.staffKey,
      groupKey
    });
    if (userIsGroupAdmin) {
      const groupMemberAdded = await this.groupsDirectory.addMember({
        staffKey,
        groupKey,
        hasWriteAccess,
        isAdmin
      });
      return parseResponse(groupMemberAdded);
    }
  }
}

module.exports = {
  UserMutations
};
