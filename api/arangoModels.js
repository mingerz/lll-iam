//@ts-check

const { db } = require('../arangoConn/arangoConn.js');
const graphDB = db.graph('patient-healthcare');
const aql = require('arangojs').aql;

function Base({ collection }) {
  const col = db.collection(collection);
  const graphCollection = graphDB.vertexCollection(collection);

  /**
   *
   * @param {object} payload
   */
  const insertIntoCollection = async payload => {
    console.log('PAYLOAD: ', payload);
    if (Array.isArray(payload)) {
      const result = await db.query(aql`
        FOR doc IN ${payload}
          UPSERT doc
          INSERT doc
          UPDATE {}
          INTO ${col}
          RETURN NEW
      `);
      return result;
    } else {
      const result = await db.query(aql`
        UPSERT ${payload}
        INSERT ${payload}
        UPDATE {}
        INTO ${col}
        RETURN NEW
      `);
      return result;
    }
  };

  /**
   *
   * @param {object} payload
   */
  const removeFromCollection = async payload => {
    const { key } = payload;
    const result = await db.query(aql`
      REMOVE ${key} IN ${col}
    `);
    return result;
  };

  /**
   *
   * @param {{key: string}} param0
   */
  const removeVertex = async ({ key }) => {
    const deleted = await graphCollection.remove(key);
    return deleted;
  };

  /**
   *
   * @param {{from: string, to: string}} param0
   */
  const removeEdge = async ({ from, to }) => {
    const deleted = await db.query(aql`
      FOR doc IN ${col}
        FILTER doc._to == ${to}
        AND doc._from == ${from}
        REMOVE doc IN ${col}
        RETURN OLD
    `);
    return deleted;
  };

  /**
   *
   * @param {{from: string, to: string}} param0
   */
  const createEdge = async ({ from, to }) => {
    const result = await db.query(aql` INSERT {_from: ${from}, _to: ${to}} INTO ${col}`);
    return result;
  };

  /**
   *
   * @param {{from: string, to: string, hasWriteAccess: boolean, isAdmin: boolean}} param0
   */
  const createEdges = async ({ from, to, hasWriteAccess, isAdmin }) => {
    const result = await db.query(
      aql` INSERT {_from: ${from}, _to: ${to}, hasWriteAccess: ${hasWriteAccess}, isAdmin: ${isAdmin}} INTO ${col} RETURN NEW`
    );
    return result;
  };

  return Object.freeze({
    insertIntoCollection,
    removeFromCollection,
    removeVertex,
    removeEdge,
    createEdge,
    createEdges
  });
}

module.exports = {
  Base
};
