### Schema

```
<!-- Groups -->
{
    name: "Group 1",
}
```

## How to start

1. Start arangodb
2. Load the mock data `node graph2.js`
3. Test the queries on arango web viewer at `localhost:8529`

### Todos

- [ ] Figure out the right metadata schema for the edges and collections
- [ ] Comple AQL filter queries

### API Queries

1. **getStaffRoles**

```
FOR s IN staff
    FILTER s._key == "staff_1"
    RETURN s.type
```

2. **getStaffGroups**

```
FOR s IN staff
    FILTER s._key == "staff_1"
    FOR group, groupAcess IN 1..1 ANY s groupDirectory
        RETURN group
```

3. **getDoctorPatients**

```
FOR staff IN staff
    FILTER staff._key == "staff_1"
    AND staff.type == 'doctor'
    FOR cust IN OUTBOUND staff patientDirectory
        RETURN cust
```

4. **getStaffDocuments**

```
FOR s IN staff
    FILTER s._key == 'staff_1'
    FOR doc, docAccess IN 1..1 OUTBOUND s documentAccess
        RETURN {s, docs: docAccess, docDetails: doc}
```

5. **getStaffColleagues**

```
FOR account IN staff
    FILTER account._key == 'staff_1'
    FOR staff IN 1..2 ANY account groupDirectory
        OPTIONS {
            bfs: true,
            uniqueVertices: 'global',
            uniqueEdges: 'path'
        }
        RETURN staff
```

7. **getPatientDocuments**

```
FOR p IN customers
    FILTER p._key == "customer_1"
    FOR doc, docAccess IN 1..1 OUTBOUND p documentAccess
        RETURN doc
```

8. **getGroupMembers**

```

```

9. **getGroupMembersDocuments**

```
FOR s IN staff
    FILTER s._key == "staff_1"
    FOR v, groupAccess, p IN 1..2 ANY s groupDirectory
        OPTIONS {
            bfs: true,
            uniqueVertices: 'global',
            uniqueEdges: 'path'
        }
        FOR doc, docAccess IN 1..1 OUTBOUND v documentAccess
            RETURN {doc, docAccess, staffName: v._key}
```

10. **getCompanyGroups**
11. **getCompanyStaff**
12. **getCompanyCustomers**

### API Mutations

1. **upsertUser** // update and create
2. **upsertGroup** // update and create
3. **upsertDocument**
4. **deleteUser**
5. **deleteGroup**
6. **deleteDocument**

### Organisation IAM Logic

todo
