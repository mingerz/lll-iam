const { db } = require('./arangoConn/arangoConn.js');
const graph = db.graph('patient-healthcare');

const collection = graph.edgeCollection('documentAccess');
const { ColMutations, ColQueries, UserMutations } = require('./api/index.js');

function newStaff({ name, rank, type }) {
  const staffMutationApi = new UserMutations('staff', db, graph);
}

async function main() {
  //   const documentAccess = new ColMutations("documentAccess", db, graph);
  //   const res = await documentAccess.createEdge({
  //     from: "customers/customer_1",
  //     to: "documents/Doc_255"
  //   });
  //   console.log(res, "--------");

  // create staff manager
  // const staffMutationApi = new UserMutations('staff', db, graph);
  // const groupMutationApi = new ColMutations('groups', db, graph);
  // const sManager = staffMutationApi.createNewUser({
  //   name: 'Jenny',
  //   rank: 'Manager',
  //   type: 'doctor'
  // });

  // create staff assistant
  const staff2 = Object.assign(new UserMutations('staff', db, graph), {
    name: 'farma1',
    type: 'Doctor1',
    rank: 'Manger',
    userKey: ''
  });

  const staff3 = Object.assign(new UserMutations('staff', db, graph), {
    name: 'jalian',
    type: 'Doctor1',
    rank: 'Manger',
    userKey: ''
  });

  const { err1, payload: payload2 } = await staff2.createNewUser();
  const { err2, payload: payload3 } = await staff3.createNewUser();

  const staffInit2 = Object.assign(new UserMutations('staff', db, graph), {
    name: 'farma1',
    type: 'Doctor1',
    rank: 'Manger',
    userKey: payload2._key
  });

  const staffInit3 = Object.assign(new UserMutations('staff', db, graph), {
    name: 'jalian',
    type: 'Doctor1',
    rank: 'Manger',
    userKey: payload3._key
  });

  const res = await staffInit2.createDocument({ documentName: 'doc1000' });
  // console.log(res);
  const { err3, payload: group1 } = await staffInit2.createGroup({ groupName: 'G1' });
  const { err4, payload: group2 } = await staffInit2.createGroup({ groupName: 'G2' });
  // console.log('group1: ', group1);
  // console.log('group2: ', group2);

  console.log(
    await staffInit2.addGroupMember({
      staffKey: payload3._key,
      groupKey: group1._key,
      hasWriteAccess: 'false',
      isAdmin: 'false'
    })
  );
  console.log(
    await staffInit2.addGroupMember({
      staffKey: payload3._key,
      groupKey: group2._key,
      hasWriteAccess: 'false',
      isAdmin: 'false'
    })
  );
  console.log(group1);
  console.log(await staffInit2.deleteGroup({ groupKey: group1._key }));

  // const sAssistant = await staffMutationApi.createNewUser({
  //   name: 'randal',
  //   rank: 'assistant',
  //   type: 'doctor'
  // });

  // create Finance Group
  // const docGroup = await groupMutationApi.createGroup({ groupName: 'Doctors' });
  // console.log(sManager);
  // console.log(sAssistant);
  // console.log(docGroup);

  // manager create document 1
  // const
  // assistant create document 2

  // give manager access to Finance Group

  // create senior staff doctor

  // create junior staff doctor

  // add senior staff and junior staff doctor to Doctor Group A

  // create patient A and patient B

  // link patient to junior doctor

  // patient create document

  // junior doctor can get all direct patient files

  // senior doctor can get all junior doctor patient files within same group

  // create patient group called CriticalPatients

  // add patient A to group CriticalPatients

  // give manager read access to CriticalPatients group

  // give senior doctor read write access to CriticalPatients group

  // const res = await userAccess.createNewUser({
  //   name: 'Lukes',
  //   type: 'nurse',
  //   rank: 'junior'
  // });
  // console.log(res);
}

main();
