const { db, aql } = require('./arangoConn/arangoConn.js');
const { ColMutations, ColQueries } = require('./api/index.js');

// const COMPANIES_V = require('./vertices/companies.json');
// const CUSTOMERS_V = require('./vertices/customers.json');
// const DOCUMENTS_V = require('./vertices/documents.json');
// const GROUPS_V = require('./vertices/groups.json');
// const STAFF_V = require('./vertices/staff.json');
// const COMPANY_DIRECTORY = require('./edges/companyDirectory.json');
// const DOCUMENT_ACCESS = require('./edges/documentAccess.json');
// const PATIENT_DIRECTORY = require('./edges/patientDirectory.json');
// const GROUP_DIRECTORY = require('./edges/groupDirectory.json');
// const GROUP_ACCESS = require('./edges/hasGroupAccess.json');

const graph = db.graph('patient-healthcare');

function accessCollection(collection) {
  return {
    mutation: new ColMutations(collection, db, graph),
    query: new ColQueries(collection, db, graph)
  };
}

async function main() {
  const companies = accessCollection('companies');
  const customers = accessCollection('patients');
  const documents = accessCollection('documents');
  const groups = accessCollection('groups');
  const staff = accessCollection('staffs');
  const companyDirectory = accessCollection('companyDirectory');
  const documentDirectory = accessCollection('documentDirectory');
  const patientDirectory = accessCollection('staffPatientDirectory');
  const groupDirectory = accessCollection('groupDirectory');

  const a2 = await companies.mutation.emptyCollection();
  const a3 = await customers.mutation.emptyCollection();
  const a4 = await documents.mutation.emptyCollection();
  const a5 = await groups.mutation.emptyCollection();
  const a6 = await staff.mutation.emptyCollection();
  const a7 = await companyDirectory.mutation.emptyCollection();
  const a8 = await documentDirectory.mutation.emptyCollection();
  const a9 = await patientDirectory.mutation.emptyCollection();
  const a10 = await groupDirectory.mutation.emptyCollection();

  const graphExists = await graph.exists();
  console.log('Graph Exists:', graphExists);
  if (graphExists === false) {
    console.log('Graph Exists:', graphExists);
    console.log('Creating Graph:', graphExists);
    const info = await graph.create({
      edgeDefinitions: [
        {
          collection: 'companyDirectory',
          from: ['companies'],
          to: ['groups']
        },
        {
          collection: 'documentDirectory',
          from: ['documents'],
          to: ['groups', 'patients', 'staffs']
        },
        {
          collection: 'staffPatientDirectory',
          from: ['staffs'],
          to: ['patients']
        },
        {
          collection: 'groupDirectory',
          from: ['groups'],
          to: ['staffs', 'patients']
        }
      ]
    });
    console.log('Graph Created', info);
  }
  // console.log('Inserting data into arangodb');
  // await companies.mutation._insertIntoCollection(COMPANIES_V);
  // await customers.mutation._insertIntoCollection(CUSTOMERS_V);
  // await documents.mutation._insertIntoCollection(DOCUMENTS_V);
  // await groups.mutation._insertIntoCollection(GROUPS_V);
  // await staff.mutation._insertIntoCollection(STAFF_V);
  // await companyDirectory.mutation._insertIntoCollection(COMPANY_DIRECTORY);
  // await documentAccess.mutation._insertIntoCollection(DOCUMENT_ACCESS);
  // await patientDirectory.mutation._insertIntoCollection(PATIENT_DIRECTORY);
  // await groupDirectory.mutation._insertIntoCollection(GROUP_DIRECTORY);
}
main();
