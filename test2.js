const { NewUser, AuthUser } = require('./api/user');

const nu = NewUser();
const authUser = AuthUser({ userUID: '1234' });

async function main() {
  const result = await nu.createUser({ name: 'Subra', uid: '1234' });
  console.log(result);
  //   const newDoc = await authUser.createFile({ documentName: 'Doc1' });
  //   console.log(newDoc);
}

main();
